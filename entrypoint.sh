#!/bin/bash

# ABM APACHE
# version: 1.0

# interrumpe la ejecución del script en el caso de error
set -e

# vaciamos directorio /var/www/html
if [ -f /var/www/html/index.html ]; then
	rm -r /var/www/html/*
fi

# Fijarse si el directorio '/etc/apache2' esta vacio
if [ -z "$(ls -A /etc/apache2)" ]; then
	#echo "El directorio esta vacio, se copian ficheros desde '/app/apache2'"
	cp -r /app/apache2/* /etc/apache2
fi


# Modificar los valores del fichero default.conf 'ServerAlias' y 'ServerName'
sed -i 's/APPSERVERNAME/'"$APPSERVERNAME"'/' /app/apache2/sites-available/default.conf
sed -i 's/APPALIAS/'"$APPALIAS"'/' /app/apache2/sites-available/default.conf

# Copiar el 'default.conf'
cp /app/apache2/sites-available/default.conf /etc/apache2/sites-available/default.conf

# Copiar las paginas web a '/var/www/html'
cp -RT /app /var/www/html/

# Dar permisos de lectura y execución a los sitios web
chmod -R a+rx /var/www/html/
# Activar sitio
a2ensite default.conf

# Inicio de servicios
apachectl -D FOREGROUND


exec "$@"












