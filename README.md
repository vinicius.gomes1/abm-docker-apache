# TUTORIAL DOCKER

Tutorial para el despliegue de una página estática php, hosteada en Apache, a partir de Docker. 

Esta página cumple la función de una aplicación web de ABM de productos.


<br>

## Instalación

<br>

### Entornos y preparativos previos - APT y repositorio

1. Actualización de base de datos de paquetes

```
$ sudo apt-get update
```
<br>

2. Instalar dependencias y paquetes previos.

```
$ sudo apt-get install apt-transport-https ca-certificates curl
gnupg2 software-properties-common
```
<br>

3. la llave GPG oficial

```
$ sudo curl -fsSL https://download.docker.com/linux/debian/gpg |
sudo apt-key add -
```
<br>

4. Para reconocer el `add-apt-repository`

```
sudo apt-get install software-properties-common
```
<br>

5. Añadimos el repositorio ‘stable’

```
$ sudo add-apt-repository "deb [arch=amd64]
https://download.docker.com/linux/ubuntu $(lsb_release -cs)
stable"
```
<br>

6. Actualizar base de datos de paquetes

```
$ sudo apt-get update
```
<br>

### Docker Engine Community - Instalación

1. Instalar docker engine

```
$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```
<br>

2. Verificar Instalación

```
$ sudo docker run hello-world
```
<br>

## Ejecución del docker

**Crear Imágenes a partir del Dockerfile**

Bajar nuestro repositorio. Para nuestro ejemplo, lo hicimos desde el directorio /home:

```
$ git clone https://gitlab.com/vinicius.gomes1/abm-docker-apache.git
```
<br>

Creamos la imagen del contenedor Apache.

```
$ docker build -t abmdockerapache . 
```
<br>

**Corremos el correr un contenedor de servidor inverso:**

```
$ docker run -d -p 80:80 \
-v /var/run/docker.sock:/tmp/docker.sock:ro \
jwilder/nginx-proxy
```
<br>

**Correr el contedor a partir de nuestra imagen**

```
$ docker run -d --name abmapachecont \
-e APPSERVERNAME=abm.istea \
-e APPALIAS=www.abm.istea \
-v /tmp/abmapache/www:/var/www/html \
-e VIRTUAL_HOST=abm.istea \
abmdockerapache
```

**Opciones:**
`-d`: lo deja en segundo plano<br>
`--name`: para elegir el nombre del contenedor<br>
`-e`: para pasar las variables de entorno<br>
`-v`: bind de montaje de un volumen<br>
`-p`: mapeamento de puertos (<host port>:<contenedor port>)<br>

Para acceder al sitio hay que agregar el host `abm.istea`. En Windows lo encontrás en C:\Windows\System32\drivers\etc\host , y en linux debia está en `/etc/hosts`.

Luego, desde un navegador tipeás `http://abm.istea`.

<br>

## Subiendo una imagen al DockerHub

Lo prinmero es crear una imagen docker a partir de un contenedor. Está imagen puede contener modificaciones hechas despúes de que corremos el contenedor. 

```
$ docker commit abmapachecont viniciusistea/abm:Apache
```
`abmapachecont:` nombre del contenedor que usaremos como base
`viniciusistea/abm:` [nombre de la cuenta]/[nombre del repositorio]:[tag]
En nuestro caso la `tag` será el número de versionado.
<br>

Podemos ver la imagen creada con el comando abajo

```
$ docker images
```
<br>

Logueamos a nuestra cuenta de DockerHub, seguido de la password.

```
$ docker login
```
<br>

Subimos un nuevo repositorio llamado abm:

```
$ docker push viniciusistea/abm
```

`Apache:` se trata de la tag que estamos pusheando

<br>

Obs: Para pushear una nueva tag a este repositorio:

```
$ docker push viniciusistea/abm:tagname
```
<br>

Podemos bajar la imagen con el comando abajo:

```
$ docker pull viniciusistea/abm:Apache
```
<br>

O simplemente correr el contenedor directamente, mencionándola:

```
$ docker run -d --name abmapachecont \
-e APPSERVERNAME=abm.istea \
-e APPALIAS=www.abm.istea \
-v /tmp/abmapache/www:/var/www/html \
-e VIRTUAL_HOST=abm.istea \
viniciusistea/abm:Apache
```
<br>

## MariaDB con la base de datos de ABM

Para crear una imagen personalizada de un contenedor de Base de Datos, vamos a a hacerlo desde una otra perspectiva. En lugar de usar un Dockerfile y entrypoint.sh, por cuestiones didácticas vamos a demonstrar como levantar una imagen oficial de MariaDB, realizar pequeñas modificaciones y subirla al Dockerhub. Estas modificaciones no quedan guardadas en el histórico, lo que puede ser util en algunos casos. 

Corremos la imagen oficial:

```
$ docker run -d --name abmdockermariadbcont \
-e MARIADB_ALLOW_EMPTY_ROOT_PASSWORD=yes \
-v /tmp/mysql:/var/lib/mysql \
mariadb
```
<br>

Importamos la base de datos:

```
$ docker exec -i abmdockermariadbcont sh -c 'exec mysql -uroot' < /home/$USER/abm-docker-apache/eduvin.sql
```
Para nuestro ejemplo optamos por no pasar password de root en las variables de entorno. El en caso de que lo hayas hecho deberás añadir al comando `-uroot -p[passwordelegido]`.

<br>

Confirmamos que la base de datos fue importada:

```
$ docker exec -ti abmdockermariadbcont mysql -e "SHOW TABLES FROM eduvin"
```
<br>

Creamos un usuario y damos los permisos a la base de datos de nuestra aplicación:

```
$ docker exec -ti abmdockermariadbcont mysql -e "CREATE USER 'abmdbuser'@'localhost' IDENTIFIED BY 'bagaza123';"    

$ docker exec -ti abmdockermariadbcont mysql -e "GRANT ALL PRIVILEGES ON *.* TO 'abmdbuser'@'localhost';"

```
<br>

Mostrar los usuarios y sus permisos:

```
$ docker exec -ti abmdockermariadbcont mysql -e "SELECT User, Host FROM mysql.user;"

$ docker exec -ti abmdockermariadbcont mysql -e "SHOW GRANTS FOR abmdbuser@localhost"              
```
<br>

### Subimos la imagen alterada a DockerHub

Creamos una imagen a partir del contenedor activo

```
$ docker commit abmdockermariadbcont viniciusistea/abm:MariaDB
```
<br>

Logueamos en DockerHub

```
$ docekr login
```
<br>

Subimos la tag creada a nuestro repositorio

```
$ docker push viniciusistea/abm:MariaDB
```
<br>


## Método Alternativo

### Servidor Apache y MariaDB en un mismo contenedor

Para correr los 2 servidores en un mismo contenedor, levantado por Dockerfile y entrypoint.sh, verificar:

https://gitlab.com/vinicius.gomes1/abm-docker

<br>
## Comandos Importantes en Docker

<br>

Ver procesos/contenedores y sus estados:

```
$ docker ps -a
```
<br>

Listar contenedores por sus IDs:

```
$ docker ps -a -q
```
<br>

Eliminar contenedor:

```
$ docker rm <contenedor>
```
<br>

Eliminar todos los contenedores:

```
$ docker rm $(docker ps -a -q)
```

`$(docker ps -a -q)`: devuelve todos los IDs

<br>

Controlar contenedores:

```
$ docker start <container name>
$ docker stop <container name>
```
<br>

Listar imagenes: 

```
$ docker images
```
<br>

Borrar Imagenes:

```
$ docker rmi <image names>
```
<br>

Borrar todas imagenes:

a ) Parar todas los containers:

```
$ docker stop $(docker ps -a -q)
```
<br>

b ) Borrar todas las imagenes:

```
$ docker rmi $(docker images -q)
```
<br>

Executar comandos dentro de un contenedor:

```
$ docker exec -ti dockercontenedor /bin/bash 

```
<br>

Ver logs:

```
$ docker logs mariadb
```
<br>







